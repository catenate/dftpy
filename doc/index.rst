.. dftpy documentation master file

DFTpy: Density Functional Theory with Python
============================================

.. toctree::
   :maxdepth: 2

   source/contact
   source/install
   source/tutorials/tutorials
   source/releases




Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
